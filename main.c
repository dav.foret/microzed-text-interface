/*******************************************************************
  Simple program to check LCD functionality on MicroZed
  based MZ_APO board designed by Petr Porazil at PiKRON

  mzapo_lcdtest.c       - main and only file

  (C) Copyright 2004 - 2017 by Pavel Pisa
      e-mail:   pisa@cmp.felk.cvut.cz
      homepage: http://cmp.felk.cvut.cz/~pisa
      work:     http://www.pikron.com/
      license:  any combination of GPL, LGPL, MPL or BSD licenses

 *******************************************************************/

/*! \mainpage My Personal Index Page
 *
 * \section intro_sec Introduction
 *
 * This is the introduction.
 *
 * \section install_sec Installation
 *
 * \subsection step1 Step 1: Opening the box
 *
 * etc...
 */

#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "unit.h"
#include "font_types.h"
#include "unit_menu.h"
#include "utils.h"

#define WIDTH 480
#define HEIGHT 320

#define WHITE 0xffff
#define BLACK 0x0000
#define HIGHLIGHTER 0x1d2f

#define CHECK_OFFSET 0x800

#define FONT_OFFSET 0x20
#define B_PRESSED_CHECK 0x0001



int main(int argc, char *argv[])
{
    Unit units[5];
	uint16_t bg_cols[5]; 
	unsigned char *knob_mem_base = (unsigned char*)map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);;	
	unsigned char old_pos;
	old_pos = (unsigned char)*(volatile uint32_t*)(knob_mem_base + SPILED_REG_KNOBS_8BIT_o);
    //memcpy(old_pos, (volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o), 1);

	for(int i = 0; i < 5; ++i) {
		units[i].r = (unsigned char)i; 
		units[i].g = (unsigned char)i + 1;
		units[i].b = (unsigned char)i + 2;
		units[i].r_ceil = 255;
		units[i].g_ceil = 255;
		units[i].b_ceil = 255;
	}
	
	memcpy(units[0].id, "Unit number 1   ", 16);
	memcpy(units[1].id, "Unit number 2   ", 16);
	memcpy(units[2].id, "Unit number 3   ", 16);
	memcpy(units[3].id, "Unit number 4   ", 16);
	memcpy(units[4].id, "Unit number 5   ", 16);

	for(int i = 0; i < 5; ++i) {
		bg_cols[i] = 0x0000;
	}
    
    unsigned char *parlcd_mem_base;
    parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);

    if (!parlcd_mem_base) exit(1);
  
    uint16_t fb[WIDTH * HEIGHT];
	parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for(int i = 0; i < WIDTH * (HEIGHT);i++) {
  	    fb[i] = BLACK;
	    parlcd_write_data(parlcd_mem_base, fb[i]);	 	
     }
     unsigned char highlight_index = 0;
     
//////////////////////////MAIN LOOP////////////////////////////
     for(;;) {
		 
         bg_cols[highlight_index] = BLACK; //reset previous selected row background
         uint32_t knob_vals = *(volatile uint32_t*)(knob_mem_base + SPILED_REG_KNOBS_8BIT_o);
         highlight_index += get_knob_difference(old_pos, knob_vals, 'b');
         old_pos = (unsigned char)knob_vals;
         highlight_index = highlight_index % 5;
         
         if(highlight_index < 0) {
             highlight_index = 5 + highlight_index; //if index is negative, loopback into allowed values
         }
  
         
         bg_cols[highlight_index] = HIGHLIGHTER; //set color for appropriate row in bg colors array
         
         for(int i = 0; i < 5; ++i) {
             char name[17];
             memcpy(name, units[i].id, 16);
             memcpy(name + 13, "", 1);
             write_line(bg_cols[i], i, name, fb, 0);
         }
 
         for(int i = 0; i < WIDTH * (HEIGHT);i++) {
	        parlcd_write_data(parlcd_mem_base, fb[i]);	 //	write frame buffer to lcd
         }
       
		 uint32_t rgb_knobs_value = *(volatile uint32_t*)(knob_mem_base + SPILED_REG_KNOBS_8BIT_o);
		 uint32_t b_press = ((rgb_knobs_value & 0x1000000) >> 24);
		 
         if(b_press) {
			 old_pos = unit_menu_loop(knob_mem_base, (units + highlight_index), fb, parlcd_mem_base);
			 
			 for(int i = 0; i < WIDTH * (HEIGHT);i++) {
				fb[i] = BLACK;
				parlcd_write_data(parlcd_mem_base, fb[i]);	 	
			}
		 }
    }
    
    return 0;
}

      
///////////////////////////////MAIN LOOP////////////////////////////////////
