/**
 * @file utils.h
 * @author David Foret & Pavel Sillinger
 * @date May 2018
 * @brief Utility functions for peripherals
 */

#ifndef _UTILS_H
#define _UTILS_H

 /**
 * @brief Writes line into @p fb
 * @param bg_col Background color of line
 * @param row_index Row to which line is written
 * @param str String to be written
 * @param fb Frame buffer to which the line is written
 * @param col_index X axis offset inside row
 */

void write_line(
    uint16_t bg_col,
    int row_index,
    char* str,
    uint16_t* fb,
    int col_index 
);

 /**
 * @brief Calculates shorttest path to new position of knob
 * @param old_pos Old position of knob
 * @param rgb_knobs_value Current position of knobs
 * @param color Which knob to select
 * @return Returns shortest path to new knob position
 */


int get_knob_difference(
    unsigned char old_pos,
    uint32_t rgb_knobs_value,
    char color
);

#endif