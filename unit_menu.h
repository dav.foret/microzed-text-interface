/**
 * @file unit_menu.h
 * @author David Foret & Pavel Sillinger
 * @date May 2018
 * @brief Functions for menu of selected unit
 */

#ifndef _UNIT_MENU_H
#define _UNIT_MENU_H

/**
 * @brief Writes unit parameter on screen
 * @param val Value to be written
 * @param row Row on which value is to be written
 * @param fb Frame buffer to which the row is written
 */

void write_param(
    unsigned char val, 
    int row,           
     uint16_t* fb);

/**
 * @brief Enters unit menu loop
 * @param knob_mem_base Memory base of knobs
 * @param unit Pointer to unit to be displayed
 * @param fb Frame buffer to which menu is written
 * @param parlcd_mem_base Memory base of display
 * @return Returns position of blue knob to main loop
 */

unsigned char unit_menu_loop(
    unsigned char *knob_mem_base,
    Unit* unit,
    uint16_t* fb,
    unsigned char* parlcd_mem_base);

#endif
