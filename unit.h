/**
 * @file unit.h
 * @author David Foret & Pavel Sillinger
 * @date May 2018
 * @brief Struct representing a zed unit
 */

#ifndef _UNIT_H
#define _UNIT_H
#endif

/**
 * @brief Struct representing zed unit
 */

typedef struct Units {
    unsigned char id[16]; /**<  Identifier of unit*/
    unsigned char r; /**<  R color wall*/
    unsigned char g; /**<  G color wall*/
    unsigned char b; /**<  B color wall*/
    unsigned char r_ceil; /**<  R color ceiling*/
    unsigned char g_ceil; /**<  G color ceiling*/
    unsigned char b_ceil; /**<  B color ceiling*/
} Unit;