/**
 * @file unit_menu.h
 * @author David Foret & Pavel Sillinger
 * @date May 2018
 * @brief Functions for menu of selected unit
 */


#include "mzapo_regs.h"
#include "mzapo_parlcd.h"
#include "utils.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "unit.h"

#define BLACK 0x0000

#define WIDTH 480
#define HEIGHT 320

void write_param(unsigned char val, int row,  uint16_t* fb) {
    char param_line[14];
    char num[4];
    
    sprintf(param_line + 7, "%d", (int)val);
    sprintf(num, "%d", (int)val);
    //printf("%d\n", (int)val);
   
    
    if(row == 1) {
        strcpy(param_line, "R wall: ");
    } else if (row == 2) {
        strcpy(param_line, "G wall: ");
    } else if (row == 3) {
        strcpy(param_line, "B wall: ");
    } else if (row == 4) {
        strcpy(param_line, "R ceil: ");
    } else if (row == 5) {
        strcpy(param_line, "G ceil: ");
    } else if (row == 6) {
        strcpy(param_line, "B ceil: ");
    }
    sprintf(param_line + 8, "%d", (int)val);
    
    if(strlen(num) == 2) {
		param_line[10] = ' ';
        param_line[12] = ' ';
        param_line[11] = ' ';
        param_line[13] = '\0';
	} else if(strlen(num) == 1) {
	    param_line[9] = ' ';
        param_line[10] = ' ';
	    param_line[12] = ' ';
        param_line[11] = ' ';
        param_line[13] = '\0';
	}

    write_line(BLACK, row, param_line, fb, 0);
}

unsigned char unit_menu_loop(unsigned char *knob_mem_base, Unit* unit, uint16_t* fb, unsigned char* parlcd_mem_base) {
    uint32_t old_knob_vals = *(volatile uint32_t*)(knob_mem_base + SPILED_REG_KNOBS_8BIT_o);
    unsigned char old_pos_r = (unsigned char)((old_knob_vals & 0xFF0000)>>16);
    unsigned char old_pos_g = (unsigned char)((old_knob_vals & 0xFF00)>>8);
    unsigned char old_pos_b = (unsigned char)old_knob_vals;
	
	
    for(int i = 0; i < WIDTH * (HEIGHT); i++) {
  	    fb[i] = BLACK;
	    parlcd_write_data(parlcd_mem_base, fb[i]);	 	
     }
    
    char name[17];
    
    memcpy(name, unit->id, 16);
    memcpy(name + 16, "", 1);
    write_line(BLACK, 0, name, fb, 0);
    
    int armed = 1;
    uint32_t g_press = 0;
    
    for(;;) {
        if(!g_press) {
            armed = 1;
        }
        
        write_param(unit->r, 1, fb);
        write_param(unit->g, 2, fb);
        write_param(unit->b, 3, fb);
        write_param(unit->r_ceil, 4, fb);
        write_param(unit->g_ceil, 5, fb);
        write_param(unit->b_ceil, 6, fb);
        
        for(int i = 0; i < WIDTH * (HEIGHT); i++) {
	        parlcd_write_data(parlcd_mem_base, fb[i]);	 	
        }
        
        uint32_t knob_vals = *(volatile uint32_t*)(knob_mem_base + SPILED_REG_KNOBS_8BIT_o);
        
        unit->r += get_knob_difference(old_pos_r, knob_vals, 'r');
        unit->g += get_knob_difference(old_pos_g, knob_vals, 'g');
        unit->b += get_knob_difference(old_pos_b, knob_vals, 'b');
        
        old_pos_b = (unsigned char)knob_vals;
        old_pos_g = (unsigned char)((knob_vals & 0xFF00)>>8);
        old_pos_r = (unsigned char)((knob_vals & 0xFF0000)>>16);
        
        g_press = ((knob_vals & 0x2000000) >> 25);
        uint32_t r_press = ((knob_vals & 0x4000000) >> 26);
        
        if(g_press && armed == 1) {
            fprintf(stderr ,"%d %d %d\n", (int)unit->r, (int)unit->g, (int)unit->b);
            armed = 0;
        }
        
        if(r_press) {
            return (unsigned char) knob_vals;
        }
    }
}
