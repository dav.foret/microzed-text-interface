/**
 * @file utils.h
 * @author David Foret & Pavel Sillinger
 * @date May 2018
 * @brief Utility functions for peripherals
 */

#include <stdio.h>
#include <stdlib.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "unit.h"
#include "font_types.h"

#define WHITE 0xffff
#define CHECK_OFFSET 0x800
#define FONT_OFFSET 0x20
#define WIDTH 480
#define HEIGHT 320
void write_line(uint16_t bg_col, int row_index, char* str, uint16_t* fb, int col_index) {
    if(str[0] == '\0') return;
	
    int width = font_winFreeSystem14x16.width[(int)str[0] - FONT_OFFSET];
    uint16_t check = CHECK_OFFSET;

    for(int i = 0; i < width; ++i) {
        for(int j = 0; j < 16; ++j) {
            //hlavni podminka - check je zezacatku 1 uplne vlevo, v cyklu je ta 1 posouvana doprava
            if(((uint16_t)font_winFreeSystem14x16.bits[((int)str[0] - FONT_OFFSET)*16 + j]>>4&check) == check) {
                fb[row_index * WIDTH * 16 + col_index + i + j*WIDTH] = WHITE;
              
            } else {
                fb[row_index * WIDTH * 16 + col_index + i + j*WIDTH] = bg_col;
         
            }
            
        }
  
        check = check >> 1;
    }

    //funkce volana rekurzivne vzhledem k vstupnimu stringu (+ posun v radku)
    write_line(bg_col, row_index, (str+1), fb, (col_index + width));
}


int get_knob_difference(unsigned char old_pos, uint32_t rgb_knobs_value, char color) {
    unsigned char new_pos;
    
    if(color == 'r') {
        new_pos = (unsigned char)((rgb_knobs_value & 0xFF0000)>>16);    
    } else if (color == 'g') {
        new_pos = (unsigned char)((rgb_knobs_value & 0xFF00)>>8);
    } else {
        new_pos = (unsigned char)rgb_knobs_value;    
    }
    
    if(new_pos > old_pos) {
        int clockwise = new_pos - old_pos;
        int counter_clockwise = -256 + (old_pos);
        
        if(abs(counter_clockwise) > clockwise) {
            return clockwise;
        } else {
            return counter_clockwise;
        }
    } else {
        int counter_clockwise = new_pos - old_pos;
        int clockwise = 256 - (old_pos);
        
        if(abs(counter_clockwise) > clockwise) {
            return clockwise;
        } else {
            return counter_clockwise;
        }
    }
}

